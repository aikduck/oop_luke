﻿using UnityEngine;
using System.Collections;

public class Jetpack : MonoBehaviour 
{
	public float speed = 25f;
	public float fuelAmount = 100f;
	public Rigidbody jetpack;

	// Use this for initialization
	void Start () 
	{
		jetpack = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKey(KeyCode.Mouse0) && fuelAmount > 0) 
		{
			jetpack.AddForce(Vector3.up * speed);
		}
	}
}
