﻿using UnityEngine;
using System.Collections;

public class bounceBall : MonoBehaviour {

	public Rigidbody ballRB;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnCollisionEnter(Collision other)
	{
		ballRB.AddForce (Vector3.up * other.relativeVelocity.magnitude, ForceMode.Impulse);
	}
}
