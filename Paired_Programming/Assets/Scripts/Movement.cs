﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour 
{
	public Rigidbody ballRB;
	public float speed;
	// Use this for initialization
	void Start () 
	{
		ballRB = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKey (KeyCode.A))
		{
			ballRB.AddForce(Vector3.left * speed);
		}

		if (Input.GetKey (KeyCode.D))
		{
			ballRB.AddForce(Vector3.right * speed);
		}

		if (Input.GetKey (KeyCode.W))
		{
			ballRB.AddForce(Vector3.forward * speed);
		}

		if (Input.GetKey (KeyCode.S))
		{
			ballRB.AddForce(Vector3.back * speed);
		}
	}
}
