﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour 
{
	public Rigidbody bullet;
	public float speed;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		Shooting ();
	}

	public void Shooting()
	{
		if(Input.GetKey (KeyCode.Mouse1))
		{
			Rigidbody bulletShoot;
			bulletShoot = Instantiate (bullet, transform.position, Quaternion.identity) as Rigidbody;
			bulletShoot.AddForce (Vector3.forward * speed);
		}
	}
}
