﻿using UnityEngine;
using System.Collections;

public class FunctionTutorial : MonoBehaviour {

	public string name = "Luke";
	public bool isMale = true;
	public string randomSequence = "Random Sequence";
	public int age = 22;

	// Use this for initialization
	void Start ()
	{
		//printMyName();
		printId ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		checkKeypress ();
	}

	public void printMyName ()
	{
		print ("Luke");
	}

	public void printId ()
	{
		print ("Name = " + name);
		print ("Age = " + age);

		if (isMale == true)
		{
			print ("Sex = male");
		}
		else
		{
			print ("Sex = female");
		}

		print (randomSequence);
	}

	public void checkKeypress ()
	{
		if (Input.GetKeyDown (KeyCode.W) == true)
		{
			print ("W key was pressed");
			playerIsMovingText ();
		}
		if (Input.GetKey (KeyCode.W) == true)
		{
			movePlayerForward ();
		}
		if (Input.GetKeyDown (KeyCode.A) == true)
		{
			print ("A key was pressed");
		}
		if (Input.GetKey (KeyCode.A) == true)
		{
			movePlayerLeft ();
		}
		if (Input.GetKeyDown (KeyCode.S) == true)
		{
			print ("S key was pressed");
		}
		if (Input.GetKey (KeyCode.S) == true)
		{
			movePlayerBack ();
		}
		if (Input.GetKeyDown (KeyCode.D) == true)
		{
			print ("D key was pressed");
		}
		if (Input.GetKey (KeyCode.D) == true)
		{
			movePlayerRight ();
		}
		if (Input.GetKey (KeyCode.Space) == true)
		{
			jump ();
		}
	}

	public void movePlayerForward ()
	{
		transform.Translate(Vector3.forward * Time.deltaTime);
	}
	public void movePlayerBack ()
	{
		transform.Translate(Vector3.back * Time.deltaTime);
	}
	public void movePlayerLeft ()
	{
		transform.Translate(Vector3.left * Time.deltaTime);
	}
	public void movePlayerRight ()
	{
		transform.Translate(Vector3.right * Time.deltaTime);
	}
	public void jump ()
	{
		transform.Translate(Vector3.up * Time.deltaTime);
	}


	public void playerIsMovingText ()
	{
		print ("Player is moving forward");
	}

} 
