﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public Rigidbody playerRB;
	public float speed;
	public Vector3 mousePosition;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		playerMovement ();
	}

	public void playerMovement()
	{
		RaycastHit hit;

		if (Physics.Raycast (Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
		{
			mousePosition = hit.point;
		}
		playerRB.transform.LookAt (mousePosition);

		if (Input.GetKey(KeyCode.W))
		{
			playerRB.AddForce(Vector3.forward * speed);
		}
		if (Input.GetKey(KeyCode.A))
		{
			playerRB.AddForce(Vector3.left * speed);
		}
		if (Input.GetKey(KeyCode.S))
		{
			playerRB.AddForce(Vector3.back * speed);
		}
		if (Input.GetKey(KeyCode.D))
		{
			playerRB.AddForce(Vector3.right * speed);
		}
	}
}
