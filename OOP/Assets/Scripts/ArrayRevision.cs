﻿using UnityEngine;
using System.Collections;

public class ArrayRevision : MonoBehaviour
{
	public int collectiveAgesSize;
	public int currentIndex;
	public int[] collectiveAges;

	// Use this for initialization
	void Start () {
		collectiveAges = new int[collectiveAgesSize];
		setInitialvalues();
	}
	
	// Update is called once per frame
	void Update () {
		checkKeys();
	}

	public void setInitialvalues()
	{
		for (int i = 0; i < collectiveAges.Length; i++)
		{
			collectiveAges[i] = -1;
		}

	}

	public void checkKeys()
	{
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			IncreaseIndex();
		}
		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			DecreaseIndex();
		}
		if (Input.GetKeyDown(KeyCode.Return))
		{
			setAge ();
		}
	}

	public void setAge()
	{
		collectiveAges[currentIndex] = 20;
	}

	public void DecreaseIndex()
	{
		if (currentIndex > 0)
		{
			currentIndex = currentIndex - 1;
		}
		if (currentIndex == 0)
		{
			Debug.LogWarning ("Index can not be less than 0");
		}
	}

	public void IncreaseIndex()
	{
		if (currentIndex < (collectiveAges.Length - 1))
		{
			currentIndex = currentIndex + 1;
		}
		if (currentIndex == (collectiveAges.Length - 1))
		{
			Debug.LogWarning ("Index can not be greater than array size (" + (collectiveAges.Length - 1) + ")");
		}
	}
}