﻿using UnityEngine;
using System.Collections;

public class EnemySpawnScript : MonoBehaviour {

	public  Rigidbody enemy;
	public float timeSnap = 0;
	public float timeDelay;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	if (Time.time >= (timeSnap + timeDelay))
		{
			SpawnEnemy();
		}
	}

	public void SpawnEnemy()
	{
		Rigidbody enemyClone;
		enemyClone = Instantiate(enemy, transform.position, Quaternion.identity) as Rigidbody;
		timeSnap = Time.time;
	}
}
