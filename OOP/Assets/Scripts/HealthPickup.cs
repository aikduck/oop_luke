﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {

	public int recoverAmount;

	private void destroyPickup()
	{
		Destroy (gameObject);
	}

	private void addHealth (Player targetPlayer)
	{
		targetPlayer.playerStats.health += recoverAmount;
	}

	private void OnTriggerEnter (Collider other)
	{
		Player triggeredPlayer = other.gameObject.GetComponent<Player> ();

		if (triggeredPlayer != null)
		{
			addHealth (triggeredPlayer);
			destroyPickup ();
		}
	}
}
