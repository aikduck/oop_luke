﻿using UnityEngine;
using System.Collections;

public class LocalVariablePractice : MonoBehaviour {

	public string playerName;
	public string sentence;
	public GameObject lastCollidedObject;

	// Use this for initialization
	void Start () 
	{
		SetPlayerName();
	}
	
	// Update is called once per frame
	void Update () 
	{
		CreateSentence();
		CreateDungeonMessage();
	}

	public void SetPlayerName()
	{
		string tempName;
		playerName = "Player 1";
		tempName = "Player 2";
	}

	public void CreateSentence()
	{
		string sentence;

		sentence = "";

		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			sentence += "My name is " + playerName;
		}

		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			sentence += "\n";
			sentence += "One day I died, The End";
		}

		if (sentence.Length > 0)
		{
		print (sentence);
		}
	}

	public void CreateDungeonMessage()
	{
		string dungeonMessage = "";

		if (Input.GetKeyDown(KeyCode.I))
		{
			string message = "You have entered a dungeon\n";
			dungeonMessage += message;
		}
		if (Input.GetKeyDown(KeyCode.O))
		{
			string message = "You see a dragon\n";
			dungeonMessage += message;
		}
		if (Input.GetKeyDown(KeyCode.P))
		{
			string message = "You are fucked\n";
			dungeonMessage += message;
		}
		if (Input.GetKeyDown(KeyCode.Return))
		{
			print (sentence);
			dungeonMessage = "";
			sentence = "";
		}
		if (dungeonMessage.Length > 0)
		{
		sentence += dungeonMessage;
		}
	}

	void OnCollisionEnter(Collision other)
	{
		lastCollidedObject = other.gameObject;
	}
}
