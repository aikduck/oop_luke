﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct address
{
	public int number;
	public string street;
	public string suburb;
	public int postCode;
	public string state;
	public string country;
}

public class StructTutorial : MonoBehaviour
{
	public address myAddress;




	// Use this for initialization
	void Start ()
	{
		SetStartingAddress();
	}
	
	// Update is called once per frame
	void Update ()
	{
	if (Input.GetKey ("space"))
		{
			ChangeAddress ();
		}
	}

	public void SetStartingAddress()
	{
		myAddress.number = 213;
		myAddress.street = "Pacific Hwy";
		myAddress.suburb = "St Leonards";
		myAddress.postCode = 2065;
		myAddress.state = "NSW";
		myAddress.country = "Australia";
	}

	public void ChangeAddress()
	{
		myAddress.number = 123;
		myAddress.street = "Fake Street";
		myAddress.suburb = "??????";
		myAddress.postCode = 1000;
		myAddress.state = "???";
		myAddress.country = "'Merica";
	}
}
