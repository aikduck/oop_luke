﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	public Rigidbody playerRB;
	public float speed;
	public float speedReduction;
	public float jumpForce;
	private float timer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		playerMovement ();
		FallDeath ();
	}

	public void playerMovement()
	{
		playerRB.AddForce(Vector3.forward * speed);

		if (Input.GetKeyDown(KeyCode.Space))
		{
			timer = Time.time;
		}

		if (Input.GetKey(KeyCode.Space))
		{
			playerRB.AddForce (Vector3.up * jumpForce * (((Time.time - timer)*(Time.time - timer))+1), ForceMode.Acceleration);
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.transform.tag == ("Obstacle"))
		{
			speed-=speedReduction;
			Destroy (other.gameObject);
		}
	}

	public void FallDeath()
	{
		if (playerRB.transform.position.y < -5f)
		{
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
