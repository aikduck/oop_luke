﻿using UnityEngine;
using System.Collections;

public class CoinPickup : MonoBehaviour {

	public int coinValue;
	public float rotationSpeed;
	public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		Rotation ();
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			player = other.gameObject;
			AddValue ();
			DestroyCoin();
		}
	}

	public void AddValue ()
	{
		player.GetComponent<PlayerScript> ().score += coinValue;
	}

	public void DestroyCoin()
	{
		Destroy (gameObject);
	}

	private void Rotation()
	{
		gameObject.transform.Rotate (0, rotationSpeed, 0);
	}
}
