﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct keyBindings
{
	public KeyCode forwardKey;
	public KeyCode backKey;
	public KeyCode leftKey;
	public KeyCode rightKey;
	public KeyCode jumpKey;
}
[System.Serializable]
public struct stats
{
	public float health;
	public string name;
	public float movementSpeed;
	public float jumpHeight;
	public keyBindings controls;
}

public class Player : MonoBehaviour {
	
	public Rigidbody playerRb;
	public stats playerStats;
	public keyBindings playerKeyBinds;
	
	
	// Use this for initialization
	void Start () 
	{
		playerRb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		checkKeypress ();
	}
	
	public void checkKeypress ()
	{
		if (Input.GetKey (playerStats.controls.forwardKey) == true)
		{
			print ("Forward key was pressed");
			movePlayerForward ();
		}
		
		if (Input.GetKey (playerStats.controls.leftKey) == true)
		{
			print ("Left key was pressed");
			movePlayerLeft ();
		}
		
		if (Input.GetKey (playerStats.controls.backKey) == true)
		{
			print ("Back key was pressed");
			movePlayerBack ();
		}
		
		if (Input.GetKey (playerStats.controls.rightKey) == true)
		{
			print ("Right key was pressed");
			movePlayerRight ();
		}
		
		if (Input.GetKeyDown (playerStats.controls.jumpKey) == true)
		{
			jump ();
		}
	}
	
	public void movePlayerForward ()
	{
		playerRb.AddForce (Vector3.forward * playerStats.movementSpeed);
	}
	public void movePlayerBack ()
	{
		playerRb.AddForce (Vector3.back * playerStats.movementSpeed);
	}
	public void movePlayerLeft ()
	{
		playerRb.AddForce (Vector3.left * playerStats.movementSpeed);
	}
	public void movePlayerRight ()
	{
		playerRb.AddForce (Vector3.right * playerStats.movementSpeed);
	}
	public void jump ()
	{
		playerRb.AddForce (Vector3.up * playerStats.jumpHeight, ForceMode.Impulse);
	}
	
	
}
