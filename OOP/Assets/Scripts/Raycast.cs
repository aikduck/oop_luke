﻿using UnityEngine;
using System.Collections;

public class Raycast : MonoBehaviour {

	public float deployHeight;
	public float parachuteDrag;
	public GameObject parachute;
	public Rigidbody boxRB;

	// Use this for initialization
	void Start ()
	{
		boxRB = gameObject.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		RaycastHit hit;

		if (Physics.Raycast (gameObject.transform.position, Vector3.down, out hit, deployHeight)) 
		{
				ParachuteDeploy();
		}
	}

	public void ParachuteDeploy ()
	{
		parachute.SetActive (true);
		boxRB.drag = parachuteDrag;
	}
}
