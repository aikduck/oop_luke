﻿using UnityEngine;
using System.Collections;

public class ClassPractice : MonoBehaviour {

	public int number;
	public float decimalNumber;
	public bool aBoolean;
	public string aWord;
	public string anotherWord;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PrintNumber()
	{
		print (number);
	}

	public void PrintDecimalNumber()
	{
		print (decimalNumber);
	}

	public void PrintABoolean()
	{
		print (aBoolean);
	}

	public void PrintAWord()
	{
		print (aWord);
	}

	public void PrintAnotherWord()
	{
		print (anotherWord);
	}
}
