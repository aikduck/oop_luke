﻿using UnityEngine;
using System.Collections;

public class PlatformSpawn : MonoBehaviour {

	public GameObject platformPrefab;
	public Transform platformTransform;
	public float platformOffset;
	public Rigidbody playerRB;

	public void OnTriggerEnter(Collider other)
	{
		Instantiate (platformPrefab, new Vector3 (platformTransform.position.x, platformTransform.position.y, (platformTransform.position.z + (platformOffset*playerRB.velocity.magnitude))), Quaternion.identity);
	}
}
