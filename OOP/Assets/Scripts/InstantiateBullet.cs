﻿using UnityEngine;
using System.Collections;

public class InstantiateBullet : MonoBehaviour 
{
	public Rigidbody rb;
	public Transform bulletPrefab;
	public float bulletSpeed;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		shootBullet ();
	}

	void shootBullet()
	{
		if(Input.GetKeyDown (KeyCode.Return))
		{
			Rigidbody bullet;
			bullet = Instantiate(rb, transform.position, Quaternion.identity) as Rigidbody;
			bullet.AddForce(transform.forward * bulletSpeed, ForceMode.Impulse);
		}
	}
}
