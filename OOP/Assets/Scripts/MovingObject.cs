﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {

	public Rigidbody RB;
	public float speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		checkKeys ();
	}

	public void move (Vector3 direction)
	{
		RB.AddForce(direction*speed);
	}

	public void checkKeys ()
	{
		if (Input.GetKey(KeyCode.W))
		{
			move (Vector3.forward);
		}

		if (Input.GetKey(KeyCode.A))
		{
			move (Vector3.left);
		}

		if (Input.GetKey(KeyCode.S))
		{
			move (Vector3.back);
		}

		if (Input.GetKey(KeyCode.D))
		{
			move (Vector3.right);
		}
	}
}
