﻿using UnityEngine;
using System.Collections;

public class JumpScript : MonoBehaviour {

	public Rigidbody player;
	public float boostForce;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void BoostPlayer (Vector3 dir, float force)
	{
		player.AddForce (dir*force, ForceMode.Impulse);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player") {
		
			player = other.gameObject.GetComponent<Rigidbody> ();
			BoostPlayer (transform.forward, boostForce);
		}
	}
}
