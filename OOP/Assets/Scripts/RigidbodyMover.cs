﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct keyBinds
{
	public KeyCode forwardKey;
	public KeyCode backKey;
	public KeyCode leftKey;
	public KeyCode rightKey;
	public KeyCode jumpKey;
}

public class RigidbodyMover : MonoBehaviour {

	public Rigidbody playerRb;
	public float playerSpeed;
	public keyBinds playerKeyBinds;


	// Use this for initialization
	void Start () 
	{
		playerRb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		checkKeypress ();
	}

	public void checkKeypress ()
	{
		if (Input.GetKey (playerKeyBinds.forwardKey) == true)
		{
			print ("Forward key was pressed");
			movePlayerForward ();
		}

		if (Input.GetKey (playerKeyBinds.leftKey) == true)
		{
			print ("Left key was pressed");
			movePlayerLeft ();
		}

		if (Input.GetKey (playerKeyBinds.backKey) == true)
		{
			print ("Back key was pressed");
			movePlayerBack ();
		}
	
		if (Input.GetKey (playerKeyBinds.rightKey) == true)
		{
			print ("Right key was pressed");
			movePlayerRight ();
		}

		if (Input.GetKeyDown (playerKeyBinds.jumpKey) == true)
		{
			jump ();
		}
	}
	
	public void movePlayerForward ()
	{
		playerRb.AddForce (Vector3.forward * playerSpeed);
	}
	public void movePlayerBack ()
	{
		playerRb.AddForce (Vector3.back * playerSpeed);
	}
	public void movePlayerLeft ()
	{
		playerRb.AddForce (Vector3.left * playerSpeed);
	}
	public void movePlayerRight ()
	{
		playerRb.AddForce (Vector3.right * playerSpeed);
	}
	public void jump ()
	{
		playerRb.AddForce (Vector3.up * playerSpeed, ForceMode.Impulse);
	}


}
