﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Item
{
	public string name;
	public string description;
	public string useMessage;
}

public class playerInventory : MonoBehaviour
{
	public int currentIndex;
	public Item[] items;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		checkKeys();
	}

	public void checkKeys()
	{
		if (Input.GetKeyDown(KeyCode.RightBracket))
		{
			IncreaseIndex();
		}
		if (Input.GetKeyDown(KeyCode.LeftBracket))
		{
			DecreaseIndex();
		}
		if (Input.GetKeyDown(KeyCode.Space))
		{
			useItem();
		}

	}

	public void DecreaseIndex()
	{
		if (currentIndex > 0)
		{
			currentIndex = currentIndex - 1;
		}
		if (currentIndex == 0)
		{
			Debug.LogWarning ("Index can not be less than 0");
		}
	}
	
	public void IncreaseIndex()
	{
		if (currentIndex < (items.Length - 1))
		{
			currentIndex = currentIndex + 1;
		}
		if (currentIndex == (items.Length - 1))
		{
			Debug.LogWarning ("Index can not be greater than array size (" + (items.Length - 1) + ")");
		}
	}

	public void useItem()
	{
		print (items[currentIndex].useMessage);
	 }
}
