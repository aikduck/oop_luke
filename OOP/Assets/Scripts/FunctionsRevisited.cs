﻿using UnityEngine;
using System.Collections;

public class FunctionsRevisited : MonoBehaviour {

	public string myName;
	public int myAge;
	public bool myIsMale;
	public float number1;
	public float number2;
	public float result;

	// Use this for initialization
	void Start ()
	{
		setPersonalDetails("a name", 35, false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		CheckKeys();
	}

	public void setPersonalDetails(string myName, int myAge, bool myIsMale)
	{
		setName (myName);
		setAge (myAge);
		setGender(myIsMale);
	}

	public void setName(string name)
	{
		myName = name;
	}

	public void setAge(int age)
	{
		myAge = age;
	}

	public void setGender(bool isMale)
	{
		if (isMale == true)
		{
			myIsMale = true;
		}
		else
		{
			myIsMale = false;
		}
	}

	public void SubtractNumbers(float n1, float n2)
	{
		result = n1-n2;
		print ("Subtraction result: " + result);
	}

	public void AddNumbers(float n1, float n2)
	{
		result = n1+n2;
		print ("Addition result: " + result);
	}

	public void MultiplyNumbers(float n1, float n2)
	{
		result = n1*n2;
		print ("Multiplication result: " + result);
	}

	public void DivideNumbers(float n1, float n2)
	{
		result = n1/n2;
		print ("Multiplication result: " + result);
	}

	public void CheckKeys()
	{
		if (Input.GetKeyDown(KeyCode.KeypadPlus))
		{
			AddNumbers(number1, number2);
		}
		if (Input.GetKeyDown(KeyCode.KeypadMinus))
		{
			SubtractNumbers(number1, number2);
		}
		if (Input.GetKeyDown(KeyCode.KeypadMultiply))
		{
			MultiplyNumbers(number1, number2);
		}
		if (Input.GetKeyDown(KeyCode.KeypadDivide))
		{
			DivideNumbers(number1, number2);
		}
	}

}
