﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour 
{
	public NavMeshAgent agentComponent;
	public Transform target;

	// Use this for initialization
	void Awake () 
	{
		target = GameObject.FindWithTag ("Player").transform;
		agentComponent.GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		EnemyPathfinding ();
	}

	void EnemyPathfinding()
	{
		agentComponent.SetDestination (target.position);
	}
}
