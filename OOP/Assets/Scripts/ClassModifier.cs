﻿using UnityEngine;
using System.Collections;

public class ClassModifier : MonoBehaviour {

	public ClassPractice otherClass;

	// Use this for initialization
	void Start () 
	{
		otherClass.PrintABoolean ();
		otherClass.PrintAnotherWord ();
		otherClass.PrintAWord ();
		otherClass.PrintDecimalNumber ();
		otherClass.PrintNumber ();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
