﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct pokemonStats
{
	public string type;
	public int hp;
	public int attack;
	public int defense;
	public int specialAttack;
	public int specialDefense;
	public int speed;
	
}


public class Pokemon : MonoBehaviour {

	public pokemonStats pokemonStats;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
