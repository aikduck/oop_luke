﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public Rigidbody playerRB;
	public float cameraHeight;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = new Vector3(playerRB.transform.position.x, cameraHeight, playerRB.transform.position.z);

	}
}
