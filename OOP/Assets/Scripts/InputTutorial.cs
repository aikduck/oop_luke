﻿using UnityEngine;
using System.Collections;

public class InputTutorial : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.W) == true)
		{
			print ("W key was pressed");
		}
		if (Input.GetKeyDown (KeyCode.A) == true)
		{
			print ("A key was pressed");
		}
		if (Input.GetKeyDown (KeyCode.S) == true)
		{
			print ("S key was pressed");
		}
		if (Input.GetKeyDown (KeyCode.D) == true)
		{
			print ("D key was pressed");
		}
	}
}
